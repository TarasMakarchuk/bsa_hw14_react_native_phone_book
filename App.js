import React, {useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import Search from './components/search';
import ContactList from './components/contactList';
import uuid from 'react-native-uuid';

export default function App() {

	const [state, setState] = React.useState({
		contacts: [
			{
				id: uuid.v4,
				firstName: 'John',
				lastName: 'Doe',
				avatar: 'https://whatsapp-download-free.ru/wp-content/uploads/2017/09/whatsapp-avatarka.png',
				email: 'jd@gmail.com',
				phoneNumber: '+380670091234'
			}, {
				id: uuid.v4,
				firstName: 'Bob',
				lastName: 'Marley',
				avatar: 'https://uc.zone/data/avatars/o/17/17137.jpg?1554094345',
				email: 'bm@gmail.com',
				phoneNumber: '+380670091255'
			}, {
				id: uuid.v4,
				firstName: 'John',
				lastName: 'Smith',
				avatar: 'https://www.caricature-art.com/wp-content/uploads/2019/01/%D0%A8%D0%B5%D1%80%D0%BB%D0%BE%D0%BA-785x785.jpg',
				email: 'js@gmail.com',
				phoneNumber: '+380670091288'
			}, {
				id: uuid.v4,
				firstName: 'Monika',
				lastName: 'L.',
				avatar: 'https://socialniesety.ru/core/scripts/php/rectImage.php?img_id=906&width=720&height=350',
				email: 'ml@gmail.com',
				phoneNumber: '+380670091299'
			}, {
				id: uuid.v4,
				firstName: 'Monika',
				lastName: 'L.',
				avatar: 'https://socialniesety.ru/core/scripts/php/rectImage.php?img_id=906&width=720&height=350',
				email: 'ml@gmail.com',
				phoneNumber: '+380670091299'
			}, {
				id: uuid.v4,
				firstName: 'Monika',
				lastName: 'L.',
				avatar: 'https://socialniesety.ru/core/scripts/php/rectImage.php?img_id=906&width=720&height=350',
				email: 'ml@gmail.com',
				phoneNumber: '+380670091299'
			}, {
				id: uuid.v4,
				firstName: 'Monika',
				lastName: 'L.',
				avatar: 'https://socialniesety.ru/core/scripts/php/rectImage.php?img_id=906&width=720&height=350',
				email: 'ml@gmail.com',
				phoneNumber: '+380670091299'
			}, {
				id: uuid.v4,
				firstName: 'Monika',
				lastName: 'L.',
				avatar: 'https://socialniesety.ru/core/scripts/php/rectImage.php?img_id=906&width=720&height=350',
				email: 'ml@gmail.com',
				phoneNumber: '+380670091299'
			},
		]
	});

	useEffect(() => {
		setState({...state, inMemoryContacts: state.contacts});
	}, []);


	const searchContact = value => {
		const filteredContacts = state.inMemoryContacts.filter(contact => {
			let contactLowerCase = `${contact.firstName} ${contact.lastName}`.toLowerCase();
			let searchLowerCase = value.toLowerCase();
			return contactLowerCase.indexOf(searchLowerCase) > -1;
		});
		setState({ ...state, contacts: filteredContacts	});
	};

	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Search searchContact={(value)=>searchContact(value)}/>

			</View>
			<View style={styles.contactsList}>

				<ContactList
					contacts={state.contacts}
				/>

			</View>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	contactsList: {
		flex: 1,
		borderTopWidth: 0.5,
		borderTopColor: 'gray',
		scrollEnabled: true
	}
});
