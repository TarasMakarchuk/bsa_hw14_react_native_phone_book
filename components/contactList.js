import React from 'react';
import {FlatList, StyleSheet, Text, View, Image} from "react-native";
import {Icon} from 'react-native-elements';

const contactList = props => {

	return (
		<>
			<FlatList
				data={props.contacts}
				renderItem={({item}) => (
					<View style={styles.contact}>
						<Image
							source={{uri: item.avatar}}
							style={styles.avatar}
						/>
						<View style={styles.userInformation}>
							<Text style={{fontWeight: 'bold', color: 'gray'}}>
								{item.firstName} {item.lastName}
							</Text>
							<Text>
								{item.email}
							</Text>
						</View>
						<Icon
							name='info-circle'
							type='font-awesome'
							size={20}
							color='gray'
						/>
					</View>
				)}
				ListEmptyComponrnt={() => (
					<View style={styles.contactList}>
						<Text style={styles.text}>No contact found</Text>
					</View>
				)}
			/>
		</>
	)
}

const styles = StyleSheet.create({
	contact: {
		minHeight: 50,
		padding: 10,
		marginTop: 10,
		marginBottom: 10,
		marginLeft: 40,
		marginRight: 40,
		backgroundColor: 'lightgray',
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center'
	},
	contactList: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 50
	},
	avatar: {
		height: 50,
		width: 50,
		borderRadius: 50
	},
	userInformation: {
		marginLeft: 15,
		width: '65%'

	},
	text: {
		color: 'gray'
	},
});

export default contactList;
