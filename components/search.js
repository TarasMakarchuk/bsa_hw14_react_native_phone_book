import React from 'react';
import {SafeAreaView, StyleSheet, TextInput} from "react-native";

const search = props => {
	return (
		<>
			<SafeAreaView style={{backgroundColor: 'lightgray'}}/>
			<TextInput
				onChangeText={(value) => props.searchContact(value)}
				placeholder='Search'
				placeholderTextColor='white'
				style={styles.search}
			/>
		</>
	)
}

const styles = StyleSheet.create({
	search: {
		backgroundColor: 'gray',
		height: 35,
		fontSize: 18,
		padding: 10,
		color: 'white',
		margin: 50,
		borderRadius: 10,
	}
});

export default search;
